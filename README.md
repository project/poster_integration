# CONTENTS OF THIS FILE

 - Module description.
 - Configuration
 - Sync Products and Product categories.

## Description
Integration between Poster POS system and Drupal Commerce.
It synchronizes categories and products from [Poster](https://joinposter.com/en) to your Commerce store.

Commerce orders will send immediately to POS terminal.

## Configuration

 - Please go to 'admin/commerce/config/poster_integration' to configure the
   Poster integration module.

 - The module requires you to input Access Token.
   To get your access token, go to your Poster admin url
   manage/access/integration.

 - After saving access token you need to press Save configuration
   one more time to save fetched poster data (domain, currency etc.)

 - Commerce store default currency should be the same as Poster currency.

## Sync Products and Product categories.

Please go to admin/commerce/config/poster_integration/getproducts and
for the first press 'Get categories from Poster',
after that - 'Sync products from Poster to Commerce'.
