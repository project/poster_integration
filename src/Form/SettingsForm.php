<?php

namespace Drupal\poster_integration\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\poster_integration\Service\PhoneValidator;
use Drupal\poster_integration\PosterConnection;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Poster Integration settings form.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Phone Validator service.
   *
   * @var \Drupal\poster_integration\Service\PhoneValidator
   */
  protected $phoneValidator;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $storage;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\poster_integration\Service\PhoneValidator $phone_validator
   *   Phone validation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PhoneValidator $phone_validator, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->phoneValidator = $phone_validator;
    $this->storage = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('poster_integration.phone_validator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'poster_integration_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['poster_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('poster_integration.settings');

    $form['#tree'] = TRUE;
    $form['access_token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Access Token'),
      '#default_value' => $config->get('access_token'),
    ];

    if ($config->get('access_token')) {
      if (!$config->get('spot_id')) {
        $this->messenger()
          ->addError($this->t('Please press <strong>Save Configuration</strong> button to save data loaded from Poster'));
      }
      // Get spots.
      $connection = new PosterConnection();
      $decoded_result = Json::decode($connection->makeRequest('spots.getSpotTablesHalls', 'get'));
      $options = [];
      if ($decoded_result['response']) {
        foreach ($decoded_result['response'] as $value) {
          $options[$value['spot_id']] = $value['hall_name'];
        }
        $form['spot_id'] = [
          '#title' => $this->t('Spot ID'),
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => $config->get('spot_id'),
        ];
      }
      else {
        $this->messenger()
          ->addError($this->t('You need to add at least 1 spot in Poster'));
      }
      // Check currency for commerce and Poster.
      $active_currencies = $this->storage->getStorage('commerce_currency')
        ->loadByProperties([
          'status' => TRUE,
        ]);
      if (empty($active_currencies)) {
        $this->messenger()
          ->addError($this->t('Set currency in your Commerce Store'));
      }
      $decoded_result2 = Json::decode($connection->makeRequest('settings.getAllSettings', 'get'));
      $default_currency = '';
      if (isset($decoded_result2['response']['currency']['currency_code_iso']) && !empty($active_currencies)) {
        foreach ($active_currencies as $currency) {
          if ($decoded_result2['response']['currency']['currency_code_iso'] == $currency->id()) {
            $default_currency = $decoded_result2['response']['currency']['currency_code_iso'];
            break;
          }
        }
        if ($default_currency) {
          $form['currency'] = [
            '#type' => 'textfield',
            '#attributes' => ['readonly' => 'readonly'],
            '#title' => $this->t('Poster currency'),
            '#default_value' => $config->get('currency') ?? $default_currency,
          ];
        }
        else {
          $this->messenger()
            ->addError($this->t('You need add @currency to the <a href=":curlist">list of Commerce currencies.</a>', [
              '@currency' => $decoded_result2['response']['currency']['currency_code_iso'],
              ':curlist' => '/admin/commerce/config/currencies',
            ]));
        }
      }
      $result = $connection->makeRequest('settings.getAllSettings', 'get');
      $decoded_result = Json::decode($result);
      if ($decoded_result['response']) {
        // Save subdomain setting.
        $form['domain'] = [
          '#type' => 'textfield',
          '#attributes' => ['readonly' => 'readonly'],
          '#title' => $this->t('Poster subdomain'),
          '#default_value' => $config->get('domain') ?? 'https://' . $decoded_result['response']['COMPANY_ID'] . '.joinposter.com',
        ];
      }
      else {
        // Wrong credentials.
        $this->messenger()
          ->addError($this->t("You've added wrong Access Token"));
      }

    }
    $form['country'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Country for phone number validation in checkout form'),
      '#options' => $this->phoneValidator->getCountryList(),
      '#default_value' => $config->get('country'),
    ];
    $vocabularies = $this->storage->getStorage('taxonomy_vocabulary')
      ->loadMultiple();
    if ($vocabularies) {
      $vocabularies_array = [];
      foreach ($vocabularies as $value) {
        $vocabularies_array[$value->id()] = $value->get('name');
      }
      $form['catvocabulary'] = [
        '#type' => 'select',
        '#title' => $this->t('Choose vocabulary for categories import'),
        '#options' => $vocabularies_array,
        '#default_value' => $config->get('catvocabulary') ?? 'categories',
      ];
    }
    else {
      $form['vocabulary'] = [
        '#type' => 'markup',
        '#markup' => '<h3>' . $this->t('Create at least 1 vocabulary to add product categories to it.') . '</h3>',
      ];
    }
    $product_types = $this->storage->getStorage('commerce_product_type')
      ->loadMultiple();
    $product_types_array = [];
    foreach ($product_types as $value) {
      $product_types_array[$value->id()] = $value->get('label');
    }
    $form['prodtype'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose product type for product import'),
      '#options' => $product_types_array,
      '#default_value' => $config->get('prodtype') ?? 'imported',
    ];
    $form['send_orders'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send orders to Poster'),
      '#default_value' => $config->get('send_orders') ?? 1,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $poster_settings = $this->configFactory->getEditable('poster_integration.settings');
    // Set the submitted configuration setting.
    $poster_settings->set('access_token', $form_state->getValue('access_token'))
      ->set('spot_id', $form_state->getValue('spot_id'))
      ->set('country', $form_state->getValue('country'))
      ->set('catvocabulary', $form_state->getValue('catvocabulary'))
      ->set('prodtype', $form_state->getValue('prodtype'))
      ->set('currency', $form_state->getValue('currency'))
      ->set('domain', $form_state->getValue('domain'))
      ->set('send_orders', $form_state->getValue('send_orders'));
    $poster_settings->save();
    parent::submitForm($form, $form_state);
  }

}
