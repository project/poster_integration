<?php

namespace Drupal\poster_integration\Form;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\poster_integration\PosterConnection;
use Drupal\poster_integration\Service\LoadHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Batch\BatchBuilder;

/**
 * Base form to load entities from the POSTER API.
 */
abstract class LoadBaseForm extends FormBase {

  /**
   * Elements per operation.
   */
  const BATCH_LIMIT = 10;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Helper to manage loaded poster objects.
   *
   * @var \Drupal\poster_integration\Service\LoadHelper
   */
  protected $helper;

  /**
   * Provides a list of available modules.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionList;

  /**
   * Base LoadForm constructor.
   *
   * @param \Drupal\poster_integration\Service\LoadHelper $helper
   *   Helper to manage loaded poster objects.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   Provides a list of available modules.
   */
  public function __construct(LoadHelper $helper, ModuleExtensionList $extension_list) {
    $this->batchBuilder = new BatchBuilder();
    $this->helper = $helper;
    $this->extensionList = $extension_list;
  }

  /**
   * Create connect to Poster API and run request.
   *
   * @param string $method_name
   *   POSTER API method name.
   * @param string $method_type
   *   Request type (GET/POST).
   *
   * @return array|null
   *   Request result, if error - null.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function runRequest(string $method_name, string $method_type = 'get'): ?array {
    $connection = new PosterConnection();
    $result = $connection->makeRequest($method_name, $method_type);
    $decoded_result = json_decode($result);
    if (empty($decoded_result->error) && is_array($decoded_result->response)) {
      return $decoded_result->response;
    }
    else {
      $this->logger('poster_integration')->error(
        'Error in POSTER response. @error',
        [
          '@error' => print_r($decoded_result->error, TRUE),
        ]
      );
      $this->messenger()->addError($this->t('Error in POSTER response. Details: @details', ['@details' => print_r($decoded_result->error, TRUE)]));
      return NULL;
    }
  }

  /**
   * Set Batch object properties.
   *
   * @param array $items
   *   Items for the batch.
   * @param string $config_value
   *   Configuration item.
   */
  protected function setBatch(array $items, string $config_value):void {
    $this->batchBuilder
      ->setTitle($this->t('Processing'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));
    $this->batchBuilder->setFile($this->extensionList->getPath('poster_integration') . '/src/Form/LoadCategoriesForm.php');
    $this->batchBuilder->addOperation([$this, 'processItems'],
      [$items, $config_value]);
    $this->batchBuilder->setFinishCallback([$this, 'finished']);
    batch_set($this->batchBuilder->toArray());
  }

  /**
   * Processor for batch operations.
   *
   * @param array $items
   *   Batch items.
   * @param string $config_value
   *   Config value (product type, vocabulary vid etc).
   * @param array $context
   *   Batch context.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function processItems(array $items, string $config_value, array &$context) {
    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, self::BATCH_LIMIT);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != self::BATCH_LIMIT) {
          $this->processItem($item, $config_value);

          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now processing item :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Finished callback for batch.
   *
   * @param bool $success
   *   Information about the success of the batch import.
   * @param array $results
   *   Information about the results of the batch import.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the products list.
   */
  public function finished(bool $success, array $results): RedirectResponse {
    $message = $this->t('Number of items affected by batch: @count', [
      '@count' => $results['processed'],
    ]);
    $this->messenger()
      ->addStatus($message);
    return new RedirectResponse(Url::fromRoute('poster_integration.get_products', [], ['absolute' => TRUE])->toString());
  }

}
