<?php

namespace Drupal\poster_integration\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\poster_integration\Service\LoadHelper;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Form to batch load categories from POSTER API.
 */
class LoadCategoriesForm extends LoadBaseForm {

  /**
   * The terms storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs a new LoadCategoriesForm object.
   *
   * @param \Drupal\poster_integration\Service\LoadHelper $helper
   *   Helper to manage loaded poster objects.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   Provides a list of available modules.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(LoadHelper $helper, ModuleExtensionList $extension_list, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($helper, $extension_list);
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('poster_integration.load_helper'),
      $container->get('extension.list.module'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'load_categories_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Press Load Categories button to get all categories from your Poster account to your Commerce') . '</p>',
    ];
    $vocabulary = $this->config('poster_integration.settings')->get('catvocabulary');
    if ($vocabulary) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Load Categories',
      ];
      $form['vocabulary'] = [
        '#type' => 'hidden',
        '#value' => $vocabulary,
      ];
    }
    else {
      $form['vocabulary'] = [
        '#type' => 'markup',
        '#markup' => '<h3>' . $this->t('Set vocabulary for categories import on settings page') . '</h3>',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $vid = $this->config('poster_integration.settings')->get('catvocabulary');
    if (!$categories = $this->runRequest('menu.getCategories')) {
      return NULL;
    }
    $this->setBatch($categories, $vid);
  }

  /**
   * Process single item.
   *
   * @param object $category
   *   Taxonomy category object.
   * @param string $vid
   *   Taxonomy Vocabulary VID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function processItem(object $category, string $vid): void {
    $taxonomy_id = $this->helper->getTaxonomyByPosterId($category->category_id);
    $this->helper->saveTaxonomy($category, $vid, $taxonomy_id);
  }

}
