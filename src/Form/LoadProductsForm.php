<?php

namespace Drupal\poster_integration\Form;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to batch load products from POSTER API.
 */
class LoadProductsForm extends LoadBaseForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('poster_integration.load_helper'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'load_products_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Press Load Products button to get all products from your Poster account to your Commerce') . '</p>',
    ];
    $prodtype = $this->config('poster_integration.settings')->get('prodtype');
    if ($prodtype) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Load Products',
      ];
      $form['prodtype'] = [
        '#type' => 'hidden',
        '#value' => $prodtype,
      ];
    }
    else {
      $form['prodtype'] = [
        '#type' => 'markup',
        '#markup' => '<h3>' . $this->t('Set Product type for products import on settings page') . '</h3>',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $prodtype = $this->config('poster_integration.settings')->get('prodtype');
    if (!$products = $this->runRequest('menu.getProducts')) {
      return NULL;
    }
    $this->setBatch($products, $prodtype);
  }

  /**
   * Process single item.
   *
   * @param object $product
   *   Array of products.
   * @param string $prodtype
   *   Product type.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function processItem(object $product, string $prodtype): void {
    $status = $this->helper->saveProduct($product, $prodtype);
    if (!$status) {
      $this->messenger()
        ->addError($this->t('Product @id import error', [
          '@id' => $product->product_id,
        ]));
    }
  }

}
