<?php

namespace Drupal\poster_integration\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\poster_integration\PosterConnection;

/**
 * Event Subscriber to send order data to Poster.
 *
 * @package Drupal\poster_integration
 */
class OrderCompleteSubscriber implements EventSubscriberInterface {

  /**
   * The Poster configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * Constructs a new OrderCompleteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Logger\LoggerChannel $logger
   *   Logger service.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    LoggerChannel $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('poster_integration.settings');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['orderCompleteHandler'];

    return $events;
  }

  /**
   * Called whenever the commerce_order.place.post_transition is dispatched.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Event for subscribe.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function orderCompleteHandler(WorkflowTransitionEvent $event) {
    /**
     * @var \Drupal\commerce_order\Entity\OrderInterface $order
     */
    if ($this->config->get('send_orders')) {
      try {
        $order = $event->getEntity();
        // Order items in the cart.
        $items = $order->getItems();
        if ($billing_info = $order->getBillingProfile()) {
          $customer = $billing_info->get('address');
        }
        else {
          throw new \RuntimeException(sprintf('Enable please Collect Billing Information option for "%s" payment gateway',
            $order->get('payment_gateway')->getValue()[0]['target_id']));
        }
        // Get products from order.
        $products = [];
        if ($items) {
          foreach ($items as $value) {
            $products[] = [
              'product_id' => $value->getPurchasedEntity()
                ->get('sku')
                ->getValue()[0]['value'],
              'count' => intval($value->get('quantity')
                ->getValue()[0]['value']),
            ];
          }
          $incoming_order = [
            'spot_id' => $this->config->get('spot_id'),
            'phone' => $customer->getValue()[0]['family_name'],
            'first_name' => $customer->getValue()[0]['given_name'],
            'address' => $customer->getValue()[0]['given_name'] . ' '
            . $customer->getValue()[0]['address_line1'] . ' '
            . $customer->getValue()[0]['family_name'],
            'products' => $products,
          ];
          $connection = new PosterConnection();
          $result
            = $connection->makeRequest('incomingOrders.createIncomingOrder',
            'post', $incoming_order);
          $decoded_result = json_decode($result);
          if (!empty($decoded_result->error)) {
            $this->logger->error(
              'The order @id generates error during Poster API request. @result',
              [
                '@id' => $order->id(),
                '@result' => $result,
              ]
            );
          }
          else {
            // @todo add custom state for orders pushed to Poster
            // $order->getState()->applyTransitionById('push');
            // $order->save();
            $this->logger->debug(
              'The order @id has been posted to Poster. @result',
              [
                '@id' => $order->id(),
                '@result' => $result,
              ]
            );
          }
        }
      }
      catch (\Exception $e) {
        $this->logger->error("Poster API error: @message",
          ['@message' => $e->getMessage()]);
      }
    }
  }

}
