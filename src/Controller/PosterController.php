<?php

namespace Drupal\poster_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\poster_integration\PosterConnection;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Controller to output the table with imported Products.
 */
class PosterController extends ControllerBase {

  /**
   * Get list of Products.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getProducts(): array {
    $connection = new PosterConnection();
    $response = $connection->makeRequest('menu.getProducts', 'get', ['type' => 'products']);
    $rows = [];
    $product_variations = [];
    $decoded = Json::decode($response);
    if (!empty($decoded['error'])) {
      $this->getLogger('poster_integration')->error(
        'Error in POSTER response. @error',
        [
          '@error' => print_r($decoded['error'], TRUE),
        ]
      );
      $this->messenger()->addError($this->t('Error in POSTER response. Details: @details', ['@details' => print_r($decoded['error'], TRUE)]));
      return [];
    }
    $header = [
      'id' => $this->t('Product ID'),
      'productid' => $this->t('Commerce product ID'),
      'name' => $this->t('Product name'),
      'product' => $this->t('Commerce product'),
      'price' => $this->t('Price'),
      'category' => $this->t('Category'),
    ];
    $entities = $this->entityTypeManager()
      ->getStorage('commerce_product_variation')
      ->loadMultiple();
    foreach ($entities as $product_variation) {
      $product_variations[$product_variation->get('sku')
        ->getValue()[0]['value']] = [
          $product_variation->get('product_id')
            ->getValue()[0]['target_id'],
          $product_variation->get('title')->getValue()[0]['value'],
          $product_variation->get('sku')->getValue()[0]['value'],
        ];
    }
    foreach ($decoded['response'] as $value) {
      $rows[] = [
        $value['product_id'],
        $product_variations[$value['product_id']][2],
        $value['product_name'],
        $product_variations[$value['product_id']] ? Link::fromTextAndUrl($product_variations[$value['product_id']][1], Url::fromRoute('entity.commerce_product.canonical', ['commerce_product' => $product_variations[$value['product_id']][0]])) : $this->t('No Commerce product'),
        $value['price'][1],
        $value['category_name'],
      ];
    }
    $array_column = array_column($rows, 0);
    array_multisort($array_column, SORT_ASC, $rows);
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }

}
