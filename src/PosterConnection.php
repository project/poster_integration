<?php

namespace Drupal\poster_integration;

/**
 * Creates connection to the Poster API.
 *
 * @package Drupal\poster_integration
 */
class PosterConnection {

  const ENDPOINT_URL = 'https://joinposter.com/api/';

  /**
   * Configuration data.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config = NULL;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * PosterConnection constructor.
   */
  public function __construct() {
    $this->config = \Drupal::config('poster_integration.settings');
    $this->httpClient = \Drupal::httpClient();
  }

  /**
   * Make request to Poster API.
   *
   * @param string $method
   *   Method name. See https://dev.joinposter.com/en/docs/v3/web/index.
   * @param string $type
   *   Request type (get/post).
   * @param array $options
   *   Additional options.
   *
   * @return string
   *   Response data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function makeRequest(string $method, string $type = 'get', array $options = []): string {
    $url = self::ENDPOINT_URL . $method . '?token=' . $this->config->get('access_token') . '&format=json';
    try {
      if ($type == 'post' || $type == 'put') {
        $optionsres = [
          'headers' => [
            'Content-Type' => 'application/json',
          ],
          'json' => $options,
        ];
        $response = $this->httpClient->post($url, $optionsres);
      }
      else {
        $response = $this->httpClient->get($url, $options);
      }
      if (empty($response)) {
        throw new \Exception('Empty Response');
      }
    }
    catch (\Exception $e) {
      watchdog_exception('poster_integration', $e);
    }
    return (isset($response) && !empty($response)) ? (string) $response->getBody() : '';
  }

}
