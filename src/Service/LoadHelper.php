<?php

namespace Drupal\poster_integration\Service;

use Drupal\commerce_price\Price;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\file\FileRepository;
use Drupal\file\FileInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Helper to manage loaded poster objects.
 */
class LoadHelper {

  use StringTranslationTrait;

  /**
   * Subdirectory in public:// to save image files.
   */
  const IMPORTED_DIR = 'imported';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * A config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */

  protected $fileSystem;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $curentUser;

  /**
   * The File repository service.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Construct our helper object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_manager,
    ConfigFactoryInterface $config_factory,
    LoggerChannel $logger,
    AccountInterface $current_user,
    FileRepository $file_repository,
    MessengerInterface $messenger,
    FileSystemInterface $file_system = NULL) {
    $this->entityTypeManager = $entity_manager;
    $this->configFactory = $config_factory->get('poster_integration.settings');
    $this->logger = $logger;
    $this->fileSystem = $file_system;
    $this->curentUser = $current_user;
    $this->messenger = $messenger;
    $this->fileRepository = $file_repository;
  }

  /**
   * Save remote file.
   *
   * @param string $url
   *   Url of the file.
   *
   * @return \Drupal\file\FileInterface|null
   *   Saved File.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveFileToField(string $url): ?FileInterface {
    if (strlen($url) > 0 && UrlHelper::isValid($url)) {
      // Check that the destination is writable.
      $temporary_directory = 'temporary://';
      if (!$this->fileSystem->prepareDirectory($temporary_directory, FileSystemInterface::MODIFY_PERMISSIONS)) {
        $this->logger->error('The directory %directory is not writable, because it does not have the correct permissions set.', ['%directory' => $this->fileSystem->realpath($temporary_directory)]);
        return NULL;
      }
      // Check that the destination is writable.
      $directory = 'public://' . self::IMPORTED_DIR;
      if (!$this->fileSystem->chmod($directory, FileSystem::CHMOD_DIRECTORY) && !$this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY)) {
        $this->logger->error('File %file could not be copied, because the destination directory %destination is not configured correctly.', [
          '%file' => $url,
          '%destination' => $this->fileSystem->realpath($directory),
        ]);
        return NULL;
      }
      $data = file_get_contents($url);
      $filename = rawurldecode($this->fileSystem->basename($url));
      return $this->fileRepository->writeData($data, $directory . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
    }
    else {
      return NULL;
    }
  }

  /**
   * Get taxonomy by Poster ID.
   *
   * @param int $id
   *   Category ID in Poster.
   *
   * @return int|null
   *   Taxonomy TID for related Poster ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTaxonomyByPosterId(int $id): ?int {
    $query = $this->entityTypeManager->getStorage('taxonomy_term')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('field_poster_id', $id);
    $term_id = $query->execute();
    return reset($term_id) ?? NULL;
  }

  /**
   * Get product variant by Poster ID.
   *
   * @param int $id
   *   Product ID in Poster.
   *
   * @return int|null
   *   Related commerce product ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getProductByPosterId(int $id): ?int {
    $query = $this->entityTypeManager->getStorage('commerce_product_variation')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('sku', $id);
    $prod_id = $query->execute();
    return reset($prod_id) ?? NULL;
  }

  /**
   * Create/update taxonomy term.
   *
   * @param object $data
   *   Data from POSTER.
   * @param string $vid
   *   Vocabulary VID.
   * @param int|null $tid
   *   Taxonomy term TID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveTaxonomy(object $data, string $vid, ?int $tid = NULL): void {
    $new_term = [
      'name' => $data->category_name,
      'vid' => $vid,
      'field_poster_id' => $data->category_id,
      'status' => $category->visible[0]->visible ?? 1,
    ];
    if (!empty($data->category_photo_origin)) {
      $new_term['field_image'] = $this->saveFileToField($this->configFactory->get('domain') . $data->category_photo_origin);
    }
    if ($tid && ($term = $this->entityTypeManager->getStorage('taxonomy_term')
      ->load($tid))) {
      // Update Term.
      $term->setName($new_term['name'])
        ->set('status', $new_term['status']);
      if (isset($new_term['field_image']) && $term->hasField('field_image')) {
        $term->set('field_image', $new_term['field_image']);
      }
    }
    else {
      // Create the taxonomy term.
      $term = $this->entityTypeManager->getStorage('taxonomy_term')
        ->create($new_term);
    }
    // Save the taxonomy term.
    $term->save();
  }

  /**
   * Create/update product.
   *
   * @param object $data
   *   Data from POSTER.
   * @param string $prodtype
   *   Product type.
   *
   * @return bool
   *   Save Product status.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveProduct(object $data, string $prodtype): bool {
    $currency = $this->configFactory->get('currency');
    if (empty($currency)) {
      $this->messenger->addError($this->t('Set currency for import, re-save <a href=":url">config form</a>.', [
        ':url' => Url::fromRoute('poster_integration.config')->toString(),
      ]));
      return FALSE;
    }
    $convert_price = $data->price->{1} ? substr($data->price->{1}, 0, -2) . '.' . substr($data->price->{1}, -2) : 0;
    $price = new Price($convert_price, $currency);
    $store = $this->entityTypeManager->getStorage('commerce_store')->loadDefault();
    $prod_array = [
      'uid' => $this->curentUser->id(),
      'type' => $prodtype,
      'sku' => $data->product_id,
      'title' => $data->product_name,
      'price' => $price,
    ];
    if (!empty($data->photo_origin)) {
      $prod_array['field_image'] = $this->saveFileToField($this->configFactory->get('domain') . $data->photo_origin);
    }
    if ($variation_id = $this->getProductByPosterId($data->product_id)) {
      $variation = $this->entityTypeManager->getStorage('commerce_product_variation')->load($variation_id);
      $variation->set('title', $prod_array['title'])->set('price', $price);
      if ($prod_array['field_image'] && $variation->hasField('field_image')) {
        $variation->set('field_image', $prod_array['field_image']);
      }
      $variation->save();
      $product = $variation->getProduct();
      if ($product) {
        $product->set('title', $data->product_name);
        if (($category_id = $data->menu_category_id) && $product->hasField('field_category')) {
          // Check if term existed.
          if ($category = $this->getTaxonomyByPosterId($category_id)) {
            $product->set('field_category', $this->getTaxonomyByPosterId($category));
          }
        }
        $product->save();
      }
    }
    elseif ($variation = $this->entityTypeManager->getStorage('commerce_product_variation')->create($prod_array)) {
      $variation->save();
      $pr_array = [
        'type' => $prodtype,
        'title' => $data->product_name,
        'variations' => [$variation],
        'stores' => [$store],
      ];
      $product = $this->entityTypeManager->getStorage('commerce_product')->create($pr_array);
      $product->save();
    }
    return TRUE;
  }

}
